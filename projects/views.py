from django.shortcuts import render, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import CreateProject


@login_required
def list_of_all_projects(request):
    list_of_projects = Project.objects.filter(members=request.user)
    context = {"list_of_projects": list_of_projects}

    return render(request, "projects/list.html", context)


@login_required
def detail_of_project(request, pk):
    instance = Project.objects.get(pk=pk)
    context = {"project_instance_details": instance}
    return render(request, "projects/detail.html", context)


@login_required
def create_new_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            form.save(commit=False)
            form.member = request.user
            instance = form.save()
            pk = instance.pk
            return redirect("show_project", pk)
    else:
        form = CreateProject()

    context = {"form": form}
    return render(request, "projects/create.html", context)
