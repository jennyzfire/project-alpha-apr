from django.urls import path
from .views import list_of_all_projects, detail_of_project, create_new_project


urlpatterns = [
    path("", list_of_all_projects, name="list_projects"),
    path("<int:pk>/", detail_of_project, name="show_project"),
    path("create/", create_new_project, name="create_project"),
]
