from django.urls import path
from .views import add_task, list_tasks, update_task

urlpatterns = [
    path("create/", add_task, name="create_task"),
    path("mine/", list_tasks, name="show_my_tasks"),
    path("<int:pk>/complete/", update_task, name="complete_task"),
]
