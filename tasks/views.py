from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .forms import CreateTask, UpdateTask
from .models import Task


@login_required
def add_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save(commit=False)
            instance = form.save()
            pk = instance.project.pk
            return redirect("show_project", pk)
    else:
        form = CreateTask()

    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def list_tasks(request):
    my_tasks = Task.objects.filter(assignee=request.user)
    context = {"my_tasks": my_tasks}

    return render(request, "tasks/list.html", context)


@login_required
def update_task(request, pk):
    task_instance = Task.objects.get(pk=pk)
    if request.method == "POST":
        form = UpdateTask(request.POST, instance=task_instance)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = UpdateTask()

    context = {"form": form}
    return render(request, "tasks/list.html", context)
